#!/bin/bash

ABBREV=$1
EDITION=$2
TIMESTAMP=$3

DATE_START=$(date +%s)

# Using temporary files to store global variables
mkdir -p tmp
echo "$ABBREV" > tmp/abbrev.txt
echo "$EDITION" > tmp/edition.txt
echo "$TIMESTAMP" > tmp/timestamp.txt
whoami > tmp/whoami.txt

sudo echo "Abbreviation: `cat tmp/abbrev.txt`"
sudo echo "Edition: `cat tmp/edition.txt`"
sudo echo "Time Stamp: `cat tmp/timestamp.txt`"

# This script uses sudo
# It's better to be prompted for a password at the beginning than later on.
time bash provision.sh

time bash iso-download.sh
sudo cp remaster.sh /usr/local/bin # Replace the original script with a new one
sudo bash /usr/local/bin/remaster.sh

DATE_END=$(date +%s)
T_SEC=$((DATE_END-DATE_START))

echo '######################################'
echo "The new $EDITION ISO has been created!"
echo 'It is in the iso-output directory.'
echo ''
echo "Time used:"
echo "$((T_SEC/60)) minutes and $((T_SEC%60)) seconds"
echo ''

bash upload.sh
