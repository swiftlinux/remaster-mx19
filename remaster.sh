#!/bin/bash

# Original source of this script:
# https://raw.githubusercontent.com/MX-Linux/mx-remaster/master/bin/remaster.sh

# This remaster script is modified for building Swift Linux ISOs.

# -------------------------------------------------------------------------------------- #
# Script:    remaster.sh                                                                 #
# Details:   remasters antiX and possibly other Live CDs created with SquashFS           #
#                                                                                        #
# This program is distributed in the hope that it will be useful, but WITHOUT            #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS          #
# -------------------------------------------------------------------------------------- #

# Checks whether a given file system is configured.
function fs_configured {
	grep -q $1 /proc/filesystems
}

# Checks whether squshfs-tools is installed and if it's the right version (needs to be from Etch)
# (modify this for distros that are not based on Debian)
function check_squashfs-tools {
	SQUASH_STATUS=$(dpkg-query -W -f='${Status}' squashfs-tools | cut -d " " -f 2-3)
	SQUASH_VER=$(dpkg-query -W -f='${version}' squashfs-tools)
	if [[ $SQUASH_VER > 1:3.2 || $SQUASH_STATUS != "ok installed" ]]; then
		echo -n "This script needs to use squashfs-tools package from Etch, can I install it" 
		Y_or_n || {
			echo -e "Squshfs-tools is a required package. Script aborted.\n"
			exit
		}
		install_squashfs-tools
	fi	
}

# Installs Squashfs-tools (modify this for distros that don't use apt-get)
# (modify this for distros that are not based on Debian)
function install_squashfs-tools {
	apt-get update
	apt-get install squashfs-tools/stable
	if [[ $? -ne 0 ]]; then
		echo -e "Error installing required package. Script aborted.\n"
		exit
	fi
}

function create_working_dir {
	echo "STEP 3E: creating the working directory ($REM)"
	if [ -d "$REM" ] 
	then
		umount $REM
		rm -rf $REM
	fi
	mkdir $REM
}

# Sets the path to iso or cdrom
function get_iso_path {
	echo 'STAGE 3D: getting the path of the input Linux ISO file'
	CD='iso-downloaded/linux.iso'
}

function create_remaster_env {
	echo '###################################################'
	echo 'BEGIN STAGE 4: creating the remastering environment'
	echo '###################################################'
	echo "Creating directory structure for this operation"
	cd $REM
	mkdir iso squashfs new-iso new-squashfs
	echo "($REM/iso) directory to mount CD on"
	echo "($REM/squashfs) directory for old squashfs"
	echo "($REM/new-squashfs) directory for new squashfs"
	echo -e "($REM/new-iso) directory for new iso \n"
	echo -e "mounting original CD to $REM/iso"
	mount_iso $CD
	copy_iso iso new-iso
	mount_compressed_fs $SQUASH squashfs
	echo -e "Copying mounted squashfs to $REM/new-squashfs (takes some time) \n"
	cp -a squashfs/* new-squashfs/
	umount squashfs
	umount iso
	rm -r squashfs
	rm -r iso
	echo '######################################################'
	echo 'FINISHED STAGE 4: creating the remastering environment'
	echo '######################################################'
}

# Mounts ISO named $1 to $REM/iso
function mount_iso {
	cd $STARTPATH
	mount -o loop $1 $REM/iso
	if [[ $? -ne 0 ]]; then
		echo -n "Could not mount the CD image, do you want to try again"
		Y_or_n || exit 3
		$0
		exit
	fi
	cd $REM
}

# Copy cdrom content (except squash file) from $1 to $2
function copy_iso {
	# Finds the biggest file in ISO, which is most likely the squash file
	SQUASH=$(find $1 -type f -printf "%s %p\n" | sort -rn | head -n1 | cut -d " " -f 2)
	echo "Copying live CD files to $2"
	SQUASH_REL=${SQUASH#$1/}
	rsync -a $1/ $2 --exclude=$SQUASH_REL
}

# Function mounts file $1 of type $2
function mount_compressed_fs { 
	echo "Mounting original squashfs to $REM/squashfs"
	mount -t $2 -o loop $1 squashfs
	if [[ $? -ne 0 ]]; then
		umount iso
		echo "Error mounting squashfs file. \"$1\" is probable not a $2 file."
		echo "Cleaning up, removing \"remaster\" directory.\n"
		cd ..
		rm -r remaster
		exit 4
	fi
}

# Mounts all needed directories for chroot environment
function mount_all {
	# Preparing Filesystem
	mount --bind /dev $1/dev
	mount --bind /dev/pts $1/dev/pts
	mount --bind /proc $1/proc
	mount --bind /sys $1/sys
}

# Unmounts all mounted directories/files
function umount_all {
	rm -f $1/etc/hosts
	rm -f $1/etc/resolv.conf

	grep "$1" /etc/mtab >/dev/null && {
		umount $1/dev/pts
		umount $1/dev
		umount $1/sys
		umount $1/proc
	}
	grep "gshadow" /etc/mtab >/dev/null && {
		cd /
		umount $ROOTPART/etc/group
		umount $ROOTPART/etc/gshadow
		umount $ROOTPART/etc/hostname
		umount $ROOTPART/etc/hosts
		umount $ROOTPART/etc/passwd
		umount $ROOTPART/etc/shadow
		umount $ROOTPART/etc/sudoers
		umount $ROOTPART/home
		cd $REM
	}
}

# Commands that clean up the chroot environment at log out
function cleanup {
	rm -f $1/root/.synaptic/log/*
	rm -f $1/root/.bash_history
	rm -r $1/var/lib/apt/lists/*
	mkdir $1/var/lib/apt/lists/partial
}

# Mounts filesystems and chroots to remastering environment, at exit unmounts all filesystems and perform cleanup for remastering environment
function chroot_env {
	echo '#####################'
	echo 'BEGIN STAGE 5: chroot'
	echo '#####################'
	mount_all $1

	# Assume root in our new squashfs 
	chroot $1 apt-get update
	chroot $1 apt-get upgrade -y
	chroot $1 apt-get install -y keepassxc
	umount_all $1
	cleanup $1
	echo '########################'
	echo 'FINISHED STAGE 5: chroot'
	echo '########################'
}

function replace_text_in_file {
	TEXT1="$1"
	TEXT2="$2"
	FILE_TO_UPDATE="$3"
	if [[ "$FILE_TO_UPDATE" =~ "new-iso" ]]
	then
		chmod +w $FILE_TO_UPDATE
	fi
	sed -i "s|$TEXT1|$TEXT2|g" $FILE_TO_UPDATE
	if [[ "$FILE_TO_UPDATE" =~ "new-iso" ]]
	then
		chmod -w $FILE_TO_UPDATE
	fi
}

function edit_new_iso {
    echo 'STAGE 6A: updating new-iso'

    echo 'Updating new-iso/version'
	echo "$FULLNAME" > new-iso/version

	echo 'Updating new-iso/.disk/info'
	echo "$FULLNAME" > new-iso/.disk/info

	echo 'Updating new-iso/boot/grub/grub.cfg'
	replace_text_in_file 'MX-19.3 x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/grub/grub.cfg'
	replace_text_in_file 'MX-19.3' "$SHORTNAME" 'new-iso/boot/grub/grub.cfg'

	echo 'Updating new-iso/boot/grub/theme/theme.txt'
	replace_text_in_file 'MX-19.3 (patito feo)' "$FULLNAME" 'new-iso/boot/grub/theme/theme.txt'

	echo 'Updating new-iso/boot/isolinux/isolinux.cfg'
	replace_text_in_file 'MX-19.3_x64 (patito feo)' "$FULLNAME" 'new-iso/boot/isolinux/isolinux.cfg'
	replace_text_in_file 'MX-19.3_x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/isolinux/isolinux.cfg'

	echo 'Updating new-iso/boot/isolinux/readme.msg'
	replace_text_in_file 'MX-19.3_x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/isolinux/readme.msg'
	replace_text_in_file 'MX-19.3 x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/isolinux/readme.msg' # NOTE: blank space before x64

	echo 'Updating new-iso/boot/syslinux/syslinux.cfg'
	replace_text_in_file 'MX-19.3_x64 (patito feo)' "$FULLNAME" 'new-iso/boot/syslinux/syslinux.cfg'
	replace_text_in_file 'MX-19.3_x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/syslinux/syslinux.cfg' # NOTE: blank space before x64

	echo 'Updating new-iso/boot/syslinux/readme.msg'
	replace_text_in_file 'MX-19.3_x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/syslinux/readme.msg'
	replace_text_in_file 'MX-19.3 x64 (November 11, 2020)' "$FULLNAME" 'new-iso/boot/syslinux/readme.msg' # NOTE: blank space before x64
}

function edit_new_squashfs_boot {
	echo 'STAGE 6B: updating new-squashfs/boot'
	replace_text_in_file 'MX-Linux' "$EDITION" 'new-squashfs/boot/grub/themes/linen/theme.txt'
	replace_text_in_file 'MX-Linux' "$EDITION" 'new-squashfs/boot/grub/themes/mx_linux/theme.txt'
}

function edit_new_squashfs_etc {
	echo 'STAGE 6C: updating new-squashfs/etc'
	echo 'Updating new-squashfs/etc/lsb-release'
	replace_text_in_file 'MX 19.3 patito feo' "$FULLNAME $MX_VERSION" 'new-squashfs/etc/lsb-release'
	replace_text_in_file 'MX' 'Swift' 'new-squashfs/etc/lsb-release'
	echo 'Updating new-squashfs/etc/initrd-release'
	replace_text_in_file '19.3 (patito feo)' "$FULLNAME $MX_VERSION" 'new-squashfs/etc/initrd-release'
	replace_text_in_file 'MX' 'Swift' 'new-squashfs/etc/initrd-release'
}

function get_graphics {
	echo 'STAGE 6D: getting icons'
	cp $STARTPATH/icons/* 'new-squashfs/usr/local/share/icons/'
	echo 'STAGE 6E: getting desktop wallpaper'
	cp $STARTPATH/wallpaper-desktop/* 'new-squashfs/usr/share/backgrounds'
	echo 'STAGE 6F: getting login wallpaper'
	cp $STARTPATH/wallpaper-login/* 'new-squashfs/usr/share/backgrounds/login'
}

function update_desktop {
	echo 'STAGE 6G: updating the desktop'
	replace_text_in_file '/usr/share/backgrounds/mx_blue.jpg' "/usr/share/backgrounds/swiftlinux-$ABBREV.jpg" 'new-squashfs/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml'

	# Make wallpaper zoomed:
	# Resizes the image to fill the display while preserving aspect ratio.
	# If the image doesn't match the aspect ratio then it gets cropped to fit.
	replace_text_in_file '<property name="image-style" type="int" value="3"/>' '<property name="image-style" type="int" value="5"/>' 'new-squashfs/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml'
}

function update_login_screen {
	echo 'STAGE 6H: updating the login screen'
	replace_text_in_file '/usr/share/backgrounds/login/mx_blue_login.jpg' "/usr/share/backgrounds/login/swiftlinux-$ABBREV.jpg" 'new-squashfs/etc/lightdm/lightdm-gtk-greeter.conf'
	# Move the box for your login and password
	replace_text_in_file '50%,center 50%,center' '25%,center' 'new-squashfs/etc/lightdm/lightdm-gtk-greeter.conf'
}

function update_icons {
	echo 'STAGE 6I: updating the desktop icon for installing Linux'
	replace_text_in_file '/usr/share/gazelle-installer-data/logo.png' "/usr/local/share/icons/icon-$ABBREV.png" 'new-squashfs/etc/skel/Desktop/Installer.desktop'
	echo "STAGE 6J: updating MX Linux's version of the Windows Start button"
	replace_text_in_file '/usr/local/share/icons/mxfcelogo-rounded.png' "/usr/local/share/icons/icon-$ABBREV.png" 'new-squashfs/etc/skel/.config/xfce4/panel/whiskermenu-20.rc'
	echo 'STAGE 6K: updating /usr/share/gazelle-installer-data/logo.png'
	mv new-squashfs/usr/share/gazelle-installer-data/logo.png new-squashfs/usr/share/gazelle-installer-data/logo-mx.png
	cp "$STARTPATH/icons/icon-$ABBREV.png" 'new-squashfs/usr/share/gazelle-installer-data/logo.png'
}

function update_conky {
	echo 'STAGE 6L: adding Swift Linux Conky theme'
	mkdir -p 'new-squashfs/etc/skel/.conky/SwiftLinux'
	cp $STARTPATH/conky-swiftlinux-theme/* 'new-squashfs/etc/skel/.conky/SwiftLinux'
	echo 'STAGE 6M: automatically use Swift Linux Conky theme'
	replace_text_in_file 'MX-CowonBlue' 'SwiftLinux' 'new-squashfs/etc/skel/.conky/conky-startup.sh'
	replace_text_in_file 'MX-Cowon_blue_roboto' 'SwiftLinux' 'new-squashfs/etc/skel/.conky/conky-startup.sh'
}

# Set ISO path
function set_iso_path {
	echo 'STAGE 7A: setting the ISO path'
	ISOPATH=$STARTPATH/iso-output
	mkdir -p $ISOPATH
	echo -e "The ISO file will be placed by default in \"$ISOPATH\" directory. \n"
}

function set_iso_name {
	echo 'STAGE 7B: setting the ISO name'
	ISONAME="swiftlinux-$MX_VERSION-x64-$ABBREV-$TIMESTAMP.iso"
	echo "ISO name: $ISONAME"
	ISONAME=$ISOPATH/$ISONAME
}

# Create new linuxfs in the new-iso
function make_squashfs {
	echo '######################################################'
	echo 'BEGIN STAGE 8: creating the new linuxfs in the new-iso'
	echo '######################################################'
	cd $REM
	echo -e "Good. We are now creating your iso. Sit back and relax, this takes some time (some 20 minutes on an AMD +2500 for a 680MB iso). \n"
	mksquashfs $1 new-iso/antiX/linuxfs -comp xz
	if [[ $? -ne 0 ]]; then
		echo -e "Error making linuxfs file. Script aborted.\n" 
		exit 5
	fi
	echo '#########################################################'
	echo 'FINISHED STAGE 8: creating the new linuxfs in the new-iso'
	echo '#########################################################'
}

# makes iso named $1 
function make_iso {
	echo '######################################'
	echo 'BEGIN STAGE 9: making the new ISO file'
	echo '######################################'
	cd $STARTPATH
	genisoimage -l -V antiXlive -R -J -pad -no-emul-boot -boot-load-size 4 -boot-info-table -b boot/isolinux/isolinux.bin -c boot/isolinux/isolinux.cat -o $1 $REM/new-iso && isohybrid $1 $REM/new-iso
	if [[ $? -eq 0 ]]; then 
		echo
		echo "Done. You will find your very own remastered home-made Linux here: $1"
		check_size $1		
		cd $ISOPATH
		ISO_SHORT=`basename $1`
		echo 'Recording the sha256sum value of the output ISO in'
		echo "$ISOPATH/$ISO_SHORT"
		sha256sum $ISO_SHORT > $ISO_SHORT.sha256.txt
		echo "Transferring ownership of the contents of $ISOPATH"
		echo 'from root to the regular user'
		sudo chown -R $NONROOT_USER:$NONROOT_USER $ISOPATH
	else
		echo
		echo -e "ISO building failed.\n"
	fi
	cd $REM
	echo '#########################################'
	echo 'FINISHED STAGE 9: making the new ISO file'
	echo '#########################################'
}

# Displays size of created ISO file and recommends storage medium
function check_size {
	SIZE=$(ls -l $1 | cut -d " " -f 5)
	let SIZE=$SIZE/1048576 #convert in MB
	echo -n "File size = $SIZE MB, " 
	if [[ $SIZE -lt 50 ]]; then
		echo -e "you can burn this file on a business-card CD, or a larger medium\n"
	else if [[ $SIZE -lt 180 ]]; then
		echo -e "you can burn this file on a Mini CD, or a larger medium\n"
	else if [[ $SIZE -lt 650 ]]; then
		echo -e "you can burn this file on a 650 MB / 74 Min. CD, or a larger medium\n"
	else if [[ $SIZE -lt 700 ]]; then
		echo -e "you can burn this file on a 700 MB / 80 Min. CD, or a larger medium\n"
	else if [[ $SIZE -lt 4812 ]]; then 
		echo -e "this is too big for a CD, burn it on a DVD\n" 
	else if [[ $SIZE -lt  8704 ]]; then 
		echo -e "this is too big for a 4.7 GB DVD, burn it on a dual-layer DVD\n" 
	else 
		echo -e "the file is probably too big to burn even on a dual-layer DVD\n"
		fi; fi; fi; fi; fi
	fi
}

# Root check
echo 'STAGE 3A: verifying that this remaster.sh script is being executed as root'
if [[ $UID != "0" ]]; then
	echo -e "You need to be root to execute this script.\n"
	exit 1
fi

# Check that we have a squashfs file system configured.
echo 'STAGE 3B: verifying that a squashfs is installed'
fs_configured squashfs || modprobe squashfs || {
	echo
	echo "This remastering process uses the \"squashfs\" file system which doesn't seem to be installed on your system. Without it we cannot proceed. Run \"apt-get install squashfs-modules-\$(uname -r)\" \n"
	echo -e "If you do have the package installed you might need to run this command \"modprobe squashfs\" before running this script\n"
	echo -e "Script aborted.\n"
	exit 2
}

# Initializing variables
echo 'STAGE 3C: initializing variables'
STARTPATH=$PWD
ERROR=false
BUILD=false
CHROOT=false
HD=false
GENERIC=false
CUSTOM=false
MX_VERSION=`cat parameters/MX-version.txt`
ABBREV=`cat tmp/abbrev.txt`
EDITION=`cat tmp/edition.txt`
TIMESTAMP=`cat tmp/timestamp.txt`
NONROOT_USER=`cat tmp/whoami.txt`
FULLNAME="$EDITION $MX_VERSION x64 ($TIMESTAMP)"
SHORTNAME="$EDITION $MX_VERSION"
REM="$STARTPATH/remaster" # Working (remaster) directory

# This checks if squshfs-tools is installed and if it's the right version
#check_squashfs-tools

# Execute if script is NOT called with --build-iso, --chroot, --from-hdd arguments
get_iso_path $1
create_working_dir
time create_remaster_env
time chroot_env new-squashfs
edit_new_iso
edit_new_squashfs_boot
edit_new_squashfs_etc
get_graphics
update_desktop
update_login_screen
update_icons
update_conky
set_iso_path
set_iso_name
time make_squashfs new-squashfs
time make_iso $ISONAME
