# Remastering MX Linux 19
This repository contains the scripts needed to transform the latest MX Linux ISO into the various editions of Swift Linux.

## Development Environment That Works
The development environment to use is MX Linux or any similar Debian-based Linux distro.  Both physical and virtual machines work.

## Development Environments That Don't Work
* Docker does not work.  The modprobe command does not work in Docker.
* Because the development process does not work in Docker, Travis CI and other continuous integration tools don't work here.
* Vagrant is more trouble than it's worth.

## Reasons For Not Using Vagrant
* The debian/buster64 box gave me difficulty with file syncing.
* The debian/contrib-buster64 box had a peculiarity that got in the way of the remastering script.  I found that files and directories created as root were owned by the Vagrant user.  This led to permission problems.
* I could provision existing Vagrant boxes to fix them or use Packer to create a Vagrant box for Swift Linux development, but that would be reinventing the wheel, because MX Linux already creates a suitable development environment.  Trying out this repository in an MX Linux virtual machine can address the infamous "but it works on my machine" problem.

## Setup For Automated Uploads
1.  Install open-ssh with the following commands:
```
sudo apt-get update
sudo apt-get install -y openssh-client
```
2. Follow the instructions at https://sourceforge.net/p/forge/documentation/SSH%20Keys/#key-generation-openssh to generate an ssh key in your production environment and copy it into your SourceForge account.
3. Enter the command "bash test-upload.sh".  If all goes well, the upload proceeds WITHOUT a prompt for your password.

## Shell Access to SourceForge
* Enter the following command (and replace USERNAME with your actual username):
```
ssh -t USERNAME@shell.sourceforge.net create
```
* Enter the following command to go to the project directory:
```
/home/frs/project/swiftlinux
```
* Use the usual UNIX commands to move files around.

## Creating Linux ISOs (Development Environment)
* Hannah Montana Linux: Enter the command `bash build-hm.sh`.
* Interstate Swift Linux: Enter the command `bash build-interstate.sh`.
* Taylor Swift Linux: Enter the command `bash build-ts.sh`.
* Twilight Zone Swift Linux: Enter the command `bash build-tz.sh`.

## Creating Linux ISOs (Production Environment)
* Hannah Montana Linux: The cron-hm.sh script is used for this.
* Interstate Swift Linux: The cron-interstate.sh script is used for this.
* Taylor Swift Linux: The cron-ts.sh script is used for this.
* Twilight Zone Swift Linux: The cron-tz.sh script is used for this.

## Production Environment
* MX Linux and Debian Linux 10 can work as the production environment.
* Cron jobs are used to automatically build and upload new Swift Linux ISOs on a regular basis.
