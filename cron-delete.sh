#!/bin/bash

# This command is to be executed in a cron job in the cloud.

# Purpose: Refresh the MX Linux ISO.  This is necessary when MX Linux is upgraded.

git pull

rm -rf iso-downloaded
