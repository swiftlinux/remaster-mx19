#!/bin/bash

# This command is to be executed in a cron job in the cloud.

git pull

bash test-upload.sh

rm -rf iso-output # Those ISO files are big and add up.
bash build-hm.sh
