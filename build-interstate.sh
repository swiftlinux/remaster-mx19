#!/bin/bash

ABBREV='Interstate'
EDITION='Interstate Swift Linux'
TIMESTAMP=`date -u +%Y-%m%d-%H%M%S`

mkdir -p log

bash start-remaster.sh "$ABBREV" "$EDITION" "$TIMESTAMP" 2>&1 | tee log/log-$TIMESTAMP.txt
