#!/bin/bash

echo '###########################'
echo 'BEGIN STAGE 1: provisioning'
echo '###########################'

sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y bind9 # Needed for Internet connection in chroot
sudo apt-get install -y wget # Needed to download Linux ISO
sudo apt-get install -y rsync # Needed for remastering and uploading
sudo apt-get install -y squashfs-tools # Needed for remastering
sudo apt-get install -y genisoimage # Needed for remastering
sudo apt-get install -y syslinux-utils # Needed for isohybrid

echo '##############################'
echo 'FINISHED STAGE 1: provisioning'
echo '##############################'
