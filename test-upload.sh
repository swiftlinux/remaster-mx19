#!/bin/bash
set -e

# This script tests the connection to SourceForge.
# To execute uploads without a prompt for your password,
# you must create an ssh key (with OpenSSH) and post it to SourceForge.
# The procedure is at:
# https://sourceforge.net/p/forge/documentation/SSH%20Keys/

TIMESTAMP=`date +%Y-%m%d-%H%M%S`

echo '--------------------------'
echo 'Testing the upload process'
echo ''
echo "You may be asked for jhsu802701's password to SourceForge"

mkdir -p tmp
echo "$TIMESTAMP\nTesting upload, please ignore" > tmp/test-upload.txt
rsync -avPz -e ssh tmp/test-upload.txt jhsu802701@frs.sourceforge.net:/home/frs/p/swiftlinux
rm tmp/test-upload.txt
