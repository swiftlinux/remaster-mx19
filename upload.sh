#!/bin/bash

# Initializing variables
echo 'STAGE 3C: initializing variables'
STARTPATH=$PWD
MX_VERSION=`cat parameters/MX-version.txt`
ABBREV=`cat tmp/abbrev.txt`
EDITION=`cat tmp/edition.txt`
TIMESTAMP=`cat tmp/timestamp.txt`

ISOPATH=$STARTPATH/iso-output
FILE_SMALL="swiftlinux-$MX_VERSION-x64-$ABBREV-$TIMESTAMP.iso.sha256.txt"
FILE_LARGE="swiftlinux-$MX_VERSION-x64-$ABBREV-$TIMESTAMP.iso"
PATH_SMALL="$ISOPATH/$FILE_SMALL"
PATH_LARGE="$ISOPATH/$FILE_LARGE"

echo "STAGE 10A: uploading $PATH_SMALL"
rsync -avPz -e ssh "$PATH_SMALL" jhsu802701@frs.sourceforge.net:/home/frs/p/swiftlinux/$ABBREV/testing/$MX_VERSION/

echo '######################################'
echo "BEGIN STAGE 10B: uploading $PATH_LARGE"
echo '######################################'
time rsync -avPz -e ssh "$PATH_LARGE" jhsu802701@frs.sourceforge.net:/home/frs/p/swiftlinux/$ABBREV/testing/$MX_VERSION/
echo '#########################################'
echo "FINISHED STAGE 10B: uploading $PATH_LARGE"
echo '#########################################'
