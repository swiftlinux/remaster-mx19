#!/bin/bash

MX_VERSION=`cat parameters/MX-version.txt`
SHA256_URL=https://sourceforge.net/projects/mx-linux/files/Final/MX-"$MX_VERSION"_x64.iso.sha256/download
SHA256_FILENAME='iso-downloaded/sha256.txt'
ISO_URL=https://sourceforge.net/projects/mx-linux/files/Final/MX-"$MX_VERSION"_x64.iso/download
ISO_FILENAME='iso-downloaded/linux.iso'

FINISHED='false'
i=-1

echo '##############################'
echo 'BEGIN STAGE 2: downloading ISO'
echo '##############################'

function download_files () {
	rm -rf download
	rm -rf download.*
	rm -rf 'iso-downloaded'
	mkdir 'iso-downloaded'
	wait
	echo '----------------------------'
	echo "Downloading from $SHA256_URL"
	wget "$SHA256_URL"
	mv download "$SHA256_FILENAME"
	wait
	chmod a+w "$SHA256_FILENAME"
	wait
	echo '-------------------------'
	echo "Downloading from $ISO_URL"
	echo 'NOTE: takes several minutes'
	wget "$ISO_URL" --progress=dot -e dotbytes=10M
	wait
	mv download "$ISO_FILENAME"
	wait
	chmod a+w "$ISO_FILENAME" # The mounting process doesn't work if the file is unwritable.
	wait
}

function check_iso () {
	if test -f "$SHA256_FILENAME" && test -f "$ISO_FILENAME"
	then
	    echo 'Checking files'
		SHA256_EXPECTED_LONG=`cat $SHA256_FILENAME`
		SHA256_EXPECTED=${SHA256_EXPECTED_LONG:0:64} # First 64 characters only
		SHA256_ACTUAL_LONG=`sha256sum $ISO_FILENAME`
		SHA256_ACTUAL=${SHA256_ACTUAL_LONG:0:64} # First 64 characters only
		echo "Expected SHA256 value: $SHA256_EXPECTED"
		echo "Actual SHA256 value: $SHA256_ACTUAL"
		if [ "$SHA256_EXPECTED" = "$SHA256_ACTUAL" ]
		then
			FINISHED='true'
			echo 'The input ISO file is ready.'
		else
			echo 'The input ISO file is not ready.'
		fi
	fi
}

while [[ "$FINISHED" = 'false' && $i -lt 4 ]]
do
	check_iso
	if [ "$FINISHED" = 'false' ]
	then
		download_files
	fi
	((i++))
done

if [ "$FINISHED" = 'false' ]
then
	echo 'Download not completed'
	exit 1
fi

echo '#################################'
echo 'FINISHED STAGE 2: downloading ISO'
echo '#################################'
